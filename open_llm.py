OPENAI_API_KEY = "" #todo add this open api key from your gpt project
HUGGING_API_KEY = ""#todo add this hugging api key from your project


# def hugging_face_hub():
#     from langchain import HuggingFaceHub
#     llm = HuggingFaceHub(repo_id = "google/flan-t5-xl", huggingfacehub_api_token = HUGGING_API_KEY)
#     print(llm("Tell me a joke about data scientist"))

# USER_INPUT = "Paris"
def prompt_template():
    USER_INPUT = "Sihanouk Ville"
    from langchain.llms import OpenAI
    from langchain import PromptTemplate
    llm = OpenAI(model_name="gpt-3.5-turbo-instruct", openai_api_key=OPENAI_API_KEY)
    template = """I am travelling to {location}. What are the top 3 things I can do while I am there. Be very 
    specific and respond as three bullet points"""
    prompt = PromptTemplate(
        input_variables=["location"],
        template=template,

    )

    final_prompt = prompt.format(location=USER_INPUT)
    print(f"LLM Output: {llm(final_prompt)}")
    return final_prompt


def long_input():
    from langchain.llms import OpenAI
    from langchain.chains import LLMChain, SimpleSequentialChain
    from langchain import PromptTemplate
    llm = OpenAI(model_name="gpt-3.5-turbo-instruct", openai_api_key=OPENAI_API_KEY)
    # first step in chain
    template = "What is the most popular city in {country} for tourists? Just return the name of the city"
    first_prompt = PromptTemplate(
        input_variables=["country"],
        template=template)
    chain_one = LLMChain(llm=llm, prompt=first_prompt)
    # second step in chain
    second_prompt = PromptTemplate(
        input_variables=["city"],
        template="What are the top three things to do in this: {city} for tourists. Just return the answer as three bullet points.", )
    chain_two = LLMChain(llm=llm, prompt=second_prompt)
    # Combine the first and the second chain
    overall_chain = SimpleSequentialChain(chains=[chain_one, chain_two], verbose=True)
    final_answer = overall_chain.run("Vietnam")


if __name__ == "__main__":
    # long_input()
    # hugging_face_hub()
    #prompt_template()
    long_input()
